# Project description #
Проект, направленный на предоставление пользователю возможности отслеживать историю прочитанных им книг. 

Пользователю доступно 3 действия:
1. добавление книги
2. просмотр всех книг
3. поиск книг по автору и по очередности прочтения

# Directory structure #

Mai_deep_learning  
|  
|--ProgramEngineering               <- Основная папка с проектом  
|`   `|--src                        <- Кодовая база  
|`   `|--ProgramEngineering.sln       <- Файл решения Visual Studio  
|`   `|--docker-compose.dcproj        <- Файл проекта Docker-compose  
|`   `|--seed.sql                     <- Файл с тестовыми данными  
|`   `|--docker-compose.override.yml  <- Конфигурационный Yml файл для Docker-compose  
|`   `|--docker-compose.yml           <- Конфигурационный Yml файл для Docker-compose  
|`   `|--nginx.conf                   <- Конфигурационный файл для nginx  
|`   `|--cert.crt                     <- Публичный ключ для ssl сертификат  
|`   `|--.dockerignore                <- Файл типа ignore для Docker   
|  
|--TestProjectForCore               <- Юнит тесты методов контроллера  
|`   `|--HomeContollerTests.cs        <- Кодовая база юнит тестов  
|`   `|--TestProjectForCore.csproj    <- Файл проекта тестов  
|  
|--Readme.md                        <- Файл описания  
|--.gitlab-ci.yml                   <- Файл конфигурации  пайплайна  
|--.gitignore                       <- Файл типа ignore для Git  


